// importo la libreriá express
var express = require('express');
// inicio express
var app = express();

// Le indicamos un parseador para el body de la petición
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Importamos los diferentes módulos de la API
var movimientos = require('./src/api/statements.js');
movimientos(app);
var cuentas = require('./src/api/accounts.js');
cuentas(app);
var usuarios = require('./src/api/users.js');
usuarios(app);
var plotly = require('./src/api/plotly.js');
plotly(app);


// el puerto será el por defecto, o bien, si hubiera problema, el 3001
var port = process.env.PORT || 3001;
// arrancamos la aplicación escuchando en el puerto.
app.listen (port);

console.log ("API techuconde escuchando en el puerto " + port);
