var MODULE_NAME = "Accounts";
// propiedades
var properties = require ('../../cfg/properties.js')
// log
var log = require ('../utils/log.js')
// Libreria para poder realizar peticiones REST con JSON
var requestJSON = require ("request-json");

// AL arrancar pedimos max id cuenta. Mejora: hacerlo con cada petición de insercción: complicación manejar respuesta asíncrona
var maxIdCuenta = -1;
getNextSequenceValue();
var lastIban = "";
var lastIdUser = -1;
var lastIdAccount = -1;
var lastBalance = 0.0;


// Exporto endpoints asociados a app para el módulo
module.exports = function(app){
  // GET: cuentas asociados a un usuario
  //  idUser: identificador de usuario (parámetro URL)
  app.get('/techuconde/v1/users/:iduser/accounts/',
    function(req,res){
      httpCLient = requestJSON.createClient (properties.baseMLabURL);

      // Recuperamos el id del usuario y creamos la parte de la URL con el tipo de consulta de Mongo
      var idUser = req.params.iduser;
      var query = 'q={"iduser" : ' + idUser + '}';

      log.add(MODULE_NAME, log.INFO, "GET /techuconde/v1/users/"+ idUser + "/accounts/");
      log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "account?" + query + "&" + properties.MLabAPIKey);

      httpCLient.get("account?" + query + "&" + properties.MLabAPIKey,
        function (err, resMLab, body){
          if (err){
            response = {
              "msg" : "Error obtenido al consultar las cuentas de un usuario"
            }
            // Ponemos estado de error
             res.status(500);
          }
          // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
          // se debe a que el usuario no se encontró
          else{
            // Hay contenido en el body ==> lo devolvemos
            if (body.length > 0){
                response = body;
                 res.status(200);
            }
            else{
                response = {
                  "msg" : "Cuentas de usuario no encontradas"
              }
               res.status(404);
            }
          }
            res.send (response);
        }
      );
    }
    )

    // GET: cuenta por ID de cuenta
    //  idUser: identificador de usuario (parámetro URL)
    //  idaccount: identificador de cuenta (parámetro URL)
    app.get('/techuconde/v1/users/:iduser/accounts/:idaccount/',
      function(req,res){
        httpCLient = requestJSON.createClient (properties.baseMLabURL);

        // Recuperamos el id del usuario y creamos la parte de la URL con el tipo de consulta de Mongo
        var idUser = req.params.iduser;
        var idAccount = req.params.idaccount;

        var query = 'q={iduser : ' + idUser + ',"_id": { "$oid" : "' + idAccount + '"}}';

        log.add(MODULE_NAME, log.INFO, "GET /techuconde/v1/users/"+ idUser + "/accounts/" + idAccount);
        log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "account?" + query + "&" + properties.MLabAPIKey);

        httpCLient.get("account?" + query + "&" + properties.MLabAPIKey,
          function (err, resMLab, body){
            if (err){
              response = {
                "msg" : "Error obtenido al consultar la cuenta"
              }
              // Ponemos estado de error
               res.status(500);
            }
            // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
            // se debe a que la cuenta no se encontró
            else{
              // Hay contenido en el body ==> lo devolvemos
              if (body.length > 0){
                  response = body;
                   res.status(200);
              }
              else{
                  response = {
                    "msg" : "Cuenta no encontrada"
                }
                 res.status(404);
              }
            }
              res.send (response);
          }
        );
      }
      )

      // POST: inserta una nueva cuenta
      //  idUser: identificador de usuario (parámetro URL)
      app.post ('/techuconde/v1/users/:iduser/accounts/',
      function (req, res){
        // Recuperamos IBAN y balance (si existiera)
        var idUser = parseInt(req.params.iduser);
        var idAccount = req.params.idAccount;
        var IBAN = req.body.IBAN;
        var balance = req.body.balance?parseFloat(req.body.balance):0;
        // Problema con primera llamada que se recibe dos veces... lo solventamos así... hay que mejorarlo
        if (lastIban == IBAN){
          return;
        }
        lastIban = IBAN;

        console.log ("Creando cuenta para IBAN: " + IBAN);

        if (!IBAN){
            response = {
              "msg" : "Error validación datos requeridos (IBAN)"
            }
            // Ponemos estado de error
             res.status(400);
             res.send (response);
        }
        // Parámetos correctos
        else{

          var newAccount = {
            "idAccount" : maxIdCuenta++,
            "iduser" : idUser,
            "IBAN" : IBAN,
            "balance" : balance
          };

          console.log ("Dando de alta cuenta "+ newAccount);
          log.add(MODULE_NAME, log.INFO, "POST /techuconde/v1/users/"+ idUser + "/accounts/. Datos cuenta: " + JSON.stringify (newAccount));
          log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "account?" + properties.MLabAPIKey);

          httpCLient = requestJSON.createClient (properties.baseMLabURL);

          // Insertamos el movimiento en Mongo vía Mlab. El objeto json como parámetro irá en el body del post
          httpCLient.post("account?" + properties.MLabAPIKey, newAccount,
            function (err, resMLab, body){
              if (err){
                response = {
                  "msg" : "Error obtenido al insertar cuenta"
                }
                // Ponemos estado de error
                 res.status(500);
              }
              // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
              // se debe a que el usuario no se encontró
              else{
                    response = {
                      "msg" : "Cuenta insertada correctamente"
                  }
                   res.status(200);
                }
              res.send (response);
            }
          );
        }
      }
    )

    // PUT: Actualiza/Elimina los datos de una  cuenta
    //  idUser: identificador de usuario (parámetro URL)
    //  idAccount: idetificador de la cuenta
    // OJO, en el body deberían venir todos los datos, porque los no enviados se pierden al actualizar la colección
    app.put ('/techuconde/v1/users/:iduser/accounts/:idaccount/',
    function (req, res){
      console.log ("actualizando o borrando cuenta");
      // Recuperamos IBAN y balance (si existieran)
      var idUser = parseInt (req.params.iduser);
      var idAccount = parseInt (req.params.idaccount);
      var IBAN = req.body.IBAN;
      var balance = parseFloat(req.body.balance);

      if (idUser == lastIdUser && idAccount == lastIdAccount &&
      lastIban == IBAN && lastBalance == balance){
        return;
      }

      lastIban = IBAN;
      lastIdUser = idUser;
      lastIdAccount = idAccount;
      lastBalance = balance;

        var dataAccount = {
          "idAccount": idAccount,
          "iduser" : idUser,
          "IBAN" : IBAN,
          "balance" : balance
        };

        // Si no se pasa IBAN se entiendo que no es actualización, sino borrado
        if (!IBAN){
          dataAccount = [];
        }

      var query = 'q={"iduser" : ' + idUser + ',"idAccount": ' + idAccount + '}';

      log.add(MODULE_NAME, log.INFO, "PUT /techuconde/v1/users/"+ idUser + "/accounts/" + idAccount + ". Datos cuenta: " + JSON.stringify (dataAccount));
      log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "account?" + query + "&" + properties.MLabAPIKey);
      console.log (dataAccount)
      console.log (query)
      httpCLient = requestJSON.createClient (properties.baseMLabURL);

      // Insertamos el movimiento en Mongo vía Mlab. El objeto json como parámetro irá en el body del post
      httpCLient.put("account?" + query + "&" +  properties.MLabAPIKey, dataAccount,
        function (err, resMLab, body){
          if (err){
            response = {
              "msg" : "Error obtenido al actualizar cuenta"
            }
            // Ponemos estado de error
             res.status(500);
          }
          // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
          // se debe a que el usuario no se encontró
          else{
              response = {
                 "msg" : "Cuenta actulizada correctamente"
              }
               res.status(200);
            }
          res.send (response);
        }
      );
    }
  )
}

// Devuelve el siguiente máximo valor del identificador de cuentas
// de cara a dar de alta nuevas cuentas. Secuencial idAccount + 1
function getNextSequenceValue (){

  var nextValue = -1;
  // Solo campo idAccount, ordenado de mayor a menor y un único registro (l=1)
  var query ='f={"idAccount":1}&s={"idAccount":-1}&l=1';
  console.log (query);
  httpCLient = requestJSON.createClient (properties.baseMLabURL);
  httpCLient.get("account?" + query + "&" + properties.MLabAPIKey,
    function (err, resMLab, body){
      if (err){
        response = {
          "msg" : "Error obtenido al consultar las cuentas de un usuario"
        }
      }
      // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
      // se debe a que el usuario no se encontró
      else{
        // Hay contenido en el body ==> lo devolvemos
        if (body.length > 0){
            response = body;
            // Asociamos el maxIdCuenta como variable global que nos permite manejar la secuencia
            maxIdCuenta = parseInt(body[0].idAccount) + 1;
        }
        else{
            response = {
              "msg" : "Cuentas no encontradas: recuperando max de cuentas"
          }
        }
      }
    }
  );
}
