var MODULE_NAME = "Plotly";
// propiedades
var properties = require ('../../cfg/properties.js')
// log
var log = require ('../utils/log.js')
// Libreria para poder realizar peticiones REST con JSON
var requestJSON = require ("request-json");

// Exporto endpoints asociados a app para el módulo
module.exports = function(app){

  app.post('/techuconde/v1/plotly',
    function(req,res){
      console.log ("Gráfica solicitada");

      var plots = req.body.plots;

      httpClient = requestJSON.createClient (properties.basePlotlyURL);

      httpClient.setBasicAuth (properties.PlotlyUser, properties.PlotlyKey);

      httpClient.headers["Content-Type"] = "application/json";
      httpClient.headers["Plotly-Client-Platform"] = "curl";

      var body = {"figure":
	       {"data":
		        [{"y": plots}],
	            "layout": {"width": 700}
	           },
             "width": 800,
             "height": 300,
             "format": "png",
             "scale": 4,
             "encoded": "True"
           };

      httpClient.post("images", body,
        function (err, resPlotly, body){
          if (err){
            response = resPlotly;
            // Ponemos estado de error
             res.status(200);
          }
          // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
          // se debe a que el usuario no se encontró
          else{
            // Hay contenido en el body ==> lo devolvemos
            if (body.length > 0){
                response = body;
                 res.status(200);
            }
            else{
                response = resPlotly;
               res.status(404);
            }
          }
            res.send (response);
        }
      );
    }
    )
  }
