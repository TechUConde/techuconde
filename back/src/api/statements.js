var MODULE_NAME = "Statements";
// propiedades
var properties = require ('../../cfg/properties.js')
// log
var log = require ('../utils/log.js')
// Libreria para poder realizar peticiones REST con JSON
var requestJSON = require ("request-json");

var lastidUser = -1;
var lastidAccount = -1;
var lastammount = -1;
var lastcomments = "";
var lasttype = "";

// Exporto endpoints asociados a app para el módulo
module.exports = function(app){

// GET: movimientos asociados a un usuario y cuenta
//  idUser: identificador de usuario (parámetro URL)
//  idaccount: identificador de cuenta (parámetro URL)
app.get('/techuconde/v1/users/:iduser/accounts/:idaccount/statements/',
  function(req,res){
    console.log ("obteniendo movimientos");
    httpCLient = requestJSON.createClient (properties.baseMLabURL);

    // Recuperamos el id del usuario y creamos la parte de la URL con el tipo de consulta de Mongo
    var idUser = req.params.iduser;
    var idAccount = req.params.idaccount;
    var query = 'q={"iduser" : ' + idUser + ', "idaccount" : ' + idAccount + '}';

    log.add(MODULE_NAME, log.INFO, "GET /techuconde/v1/users/"+ idUser + "/accounts/" + idAccount + "/statements");
    log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "statement?" + query + "&" + properties.MLabAPIKey);

    httpCLient.get("statement?" + query + "&" + properties.MLabAPIKey,
      function (err, resMLab, body){
        if (err){
          response = {
            "msg" : "Error obtenido al consultar un usuario"
          }
          // Ponemos estado de error
           res.status(500);
        }
        // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
        // se debe a que el usuario no se encontró
        else{
          // Hay contenido en el body ==> lo devolvemos
          if (body.length > 0){
              response = body;
               res.status(200);
          }
          else{
              response = {
                "msg" : "Cuentas de usuario no encontradas"
            }
             res.status(404);
          }
        }
          res.send (response);
      }
    );
  }
  )

  // POST: inserta un nuevo movimiento
  //  idUser: identificador de usuario (parámetro URL)
  //  idaccount: identificador de cuenta (parámetro URL)
  app.post ('/techuconde/v1/users/:iduser/accounts/:idaccount/statements/',
  function (req, res){
    // Recuperamos usuario y contraseña
    var idUser = parseInt(req.params.iduser);
    var idAccount = parseInt(req.params.idaccount);
    var ammount = req.body.ammount;
    var comments = req.body.comments;
    var type = req.body.type;

    // Evitamos problema de doble llamada
    if (idUser == lastidUser && idAccount == lastidAccount &&
    ammount == lastammount && comments == lastcomments && type == lasttype){
      return;
    }


    lastidUser = idUser;
    lastidAccount = idAccount;
    lastammount = ammount;
    lastcomments = comments;
    lasttype = type;

    var newStatement = {
      "iduser" : idUser,
      "idaccount" : idAccount,
      "ammount" : ammount,
      "comments" : comments,
      "type" : type,
      "date": req.body.date
    };

    console.log ("Añadiendo movimiento " + JSON.stringify (newStatement));

    log.add(MODULE_NAME, log.INFO, "POST /techuconde/v1/users/"+ idUser + "/accounts/" + idAccount + "/statements . Datos movimiento: " + JSON.stringify (newStatement));
    log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "statement?" + properties.MLabAPIKey);

    httpCLient = requestJSON.createClient (properties.baseMLabURL);

    // Insertamos el movimiento en Mongo vía Mlab. El objeto json como parámetro irá en el body del post
    httpCLient.post("statement?" + properties.MLabAPIKey, newStatement,
      function (err, resMLab, body){
        if (err){
          response = {
            "msg" : "Error obtenido al consultar un usuario"
          }
          // Ponemos estado de error
           res.status(500);
        }
        // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
        // se debe a que el usuario no se encontró
        else{
              response = {
                "msg" : "Movimiento insertado correctamente"
            }
             res.status(200);
          }
        res.send (response);
      }
    );
  }
  )

  // DELETE: elimina movimientos de una cuenta
  //  idUser: identificador de usuario (parámetro URL)
  //  idaccount: identificador de cuenta (parámetro URL)
  app.put('/techuconde/v1/users/:iduser/accounts/:idaccount/statements/',
  function (req, res){
    console.log ("borrando movimientos");
    // Recuperamos usuario y contraseña
    var idUser = req.params.iduser;
    var idAccount = req.params.idaccount;
    var query = 'q={"idaccount" : ' + idAccount + '}';
    // body vacío para forzar la eliminación
    var bodyVacio = [];

    log.add(MODULE_NAME, log.INFO, "DELETE /techuconde/v1/users/"+ idUser + "/accounts/" + idAccount + "/statements");
    log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "statement?" + query + "&" + properties.MLabAPIKey);

    httpCLient = requestJSON.createClient (properties.baseMLabURL);

    // Borramos los movimiento en Mongo vía Mlab. El objeto json como parámetro irá en el body del post
    httpCLient.put("statement?" + query + "&" + properties.MLabAPIKey, bodyVacio,
      function (err, resMLab, body){
        if (err){
          response = {
            "msg" : "Error obtenido al consultar un usuario"
          }
          // Ponemos estado de error
           res.status(500);
        }
        // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
        // se debe a que el usuario no se encontró
        else{
              response = {
                "msg" : "Movimientos eliminados correctamente"
            }
             res.status(200);
          }
        res.send (response);
      }
    );
  }
  )
}
