var MODULE_NAME = "Users";
// propiedades
var properties = require ('../../cfg/properties.js')
// log
var log = require ('../utils/log.js')
// Libreria para poder realizar peticiones REST con JSON
var requestJSON = require ("request-json");

var enciptacion = require ('../utils/encriptacion.js');

// Exporto endpoints asociados a app para el módulo
module.exports = function(app){

  // GET: datos de un usuario
  //  idUser: identificador de usuario (parámetro URL)
  app.get('/techuconde/v1/users/:iduser/',
    function(req,res){
      httpCLient = requestJSON.createClient (properties.baseMLabURL);

      // Recuperamos el id del usuario y creamos la parte de la URL con el tipo de consulta de Mongo
      var idUser = req.params.iduser;
      var query = 'q={"id" : ' + idUser + '}';

      log.add(MODULE_NAME, log.INFO, "GET /techuconde/v1/users/"+ idUser);
      log.add (MODULE_NAME, log.INFO, properties.baseMLabURL+ "user?" + query + "&" + properties.MLabAPIKey);

      httpCLient.get("user?" + query + "&" + properties.MLabAPIKey,
        function (err, resMLab, body){
          if (err){
            response = {
              "msg" : "Error obtenido al consultar los datos de un usuario"
            }
            // Ponemos estado de error
             res.status(500);
          }
          // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
          // se debe a que el usuario no se encontró
          else{
            // Hay contenido en el body ==> lo devolvemos
            if (body.length > 0){
                response = body;
                 res.status(200);
            }
            else{
                response = {
                  "msg" : "Usuario no encontrado"
              }
               res.status(404);
            }
          }
            res.send (response);
        }
      );
    }
    )

    // POST: login de usuario
    //  contraseña, email los recupero del body
    app.post ('/techuconde/v1/users/login',
    function (req, res){
      var loginInfo = {
        "email" : req.body.email,
        "password" : req.body.password
      };

      log.add(MODULE_NAME, log.INFO, "GET /techuconde/v1/login)");
      log.add (MODULE_NAME, log.INFO, "Petición de login para email :" + loginInfo.email + " contraseña: " + loginInfo.password);

      httpCLient = requestJSON.createClient (properties.baseMLabURL);

      // Construimos la consulta para recuperar usuario con ese email y password
      var id = req.params.id;
      //var query = 'q={"email" : "' + loginInfo.email + '", "password":"'+ loginInfo.password + '"}';
      var query = 'q={"email" : "' + loginInfo.email + '"}';
      var response = {"msg":"no informado"}

      httpCLient.get("user?" + query + "&" + properties.MLabAPIKey,
        function (err, resMLab, body){
          if (err){
            response = {
              "msg" : "Error obtenido al consultar un usuario"
            }
            // Ponemos estado de error
             res.status(500);
          }
          // No ha habido error: controlamos si el cuerpo tiene contenido. Si no lo tiene
          // se debe a que el usuario no se encontró
          else{
            // Hay contenido en el body ==> logamos al usuario
            if (body.length > 0){

              // Comparamos pass con password encriptada de base de datos
              var passHash = body[0].password;
              if (enciptacion.comparePassword(req.body.password, passHash)){

                var query = 'q={"id":' + body[0].id +'}';
                var putBody = '{"$set":{"logged":true}}';

                httpCLient.put("user?" + query + "&" + properties.MLabAPIKey,JSON.parse (putBody),
                function (errPut, resMLabPut, body){
                  // No implemento nada. Doy por bueno el resultado. Al ser una función
                  // manejadora de la respuesta que está dentro de otra. Esta segunda
                  // auqneu gestione el response se pierde con el primero que es el que lo
                  // llama y también gestiona su response.
                });
                response = body[0];
                res.status (200);
              }
              else{
                  response = {
                    "msg" : "Password Incorrecta"
                }
                 res.status(405);
              }
            }
              // Body vacío: no se encontró el usuario ==> devolvemos mensaje y cambiamos el estado a 404: not found
              else{
                  response = {
                    "msg" : "Usuario no encontrado"
                }
                 res.status(404);
              }
            }
          res.send (response);
        });
    }
    )


    // POST: logout de usuario
    //  idUser: identificador de usuario (parámetro URL)s
    app.get ('/techuconde/v1/users/:id/logout',
     function(req, res) {
       var query = 'q={"id": ' + req.params.id + '}';
       log.add(MODULE_NAME, log.INFO, "POST //techuconde/v1/users/logout/" + req.params.id);
       httpClient = requestJSON.createClient (properties.baseMLabURL);
       httpClient.get("user?" + query + "&" + properties.MLabAPIKey,
         function(err, resMLab, body) {
           if (body.length == 0) {
             var response = {
               "mensaje" : "Logout incorrecto, usuario no encontrado"
             }
             res.send(response);
           } else {
             query = 'q={"id" : ' + body[0].id +'}';
             var putBody = '{"$unset":{"logged":""}}'
             httpClient.put("user?" + query + "&" + properties.MLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                 var response = {
                   "msg" : "Usuario deslogado con éxito",
                   "idUsuario" : body[0].id
                 }
                 res.send(response);
               }
             )
           }
         }
       );
     }
    )
}
