var bcrypt = require('bcrypt');

const saltRounds = 10;

exports.cryptPassword = function (password){
  return bcrypt.hashSync(password, saltRounds);
}

exports.comparePassword = function (password, hash){
  if(bcrypt.compareSync(password, hash)) {
    return true;    // password match
  } else {
    return false; // pawwdord dont match
  }
}
