// propiedades
var properties = require ('../../cfg/properties.js')
// Librería para formatear la fecha
var dateFormat = require('dateformat');
// Librería para acceder al file system
var fs = require('fs');

// Niveles de traza
var ERROR = "ERROR";
var FATAL = "FATAL";
var INFO = "INFO";
var DEBUG = "DEBUG";

exports.ERROR = ERROR;
exports.FATAL = FATAL;
exports.INFO = INFO;
exports.DEBUG = DEBUG;

// Función que añade una traza de log incluyendo
//    Fecha de la insercción de la traza
//    Nombre del componente
//    Tipo del componente
//    Comentario asociado
exports.add = function(component, type, comment) {
  var date = new Date();
  var dateLong = date.getTime();
  var formattedDate = dateFormat(dateLong, "dd/mm/yyyy, HH:MM:ss");

  fs.appendFile (
    properties.logFile,
    "[" + formattedDate +"]\t[" + component +"]\t[" + type + "] - "+ comment + "\n",
    "utf8",
    function (err){
      if (err){
        console.log ("Error al escribir en el fichero de logs " + err);
      }
    }
  );
};
