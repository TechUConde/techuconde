'use strict';

const fs = require('fs');
const nunjucks = require('nunjucks');
const colors = require('../colors');

nunjucks.render('cells-coronita-base-theme-color.njk', colors, (err, result) => {
  if (err) { return console.log(err); }

  fs.writeFile('./cells-coronita-base-theme-color.html', result, (err) => {
    if (err) { console.log(err); }
    console.log('Color theme saved!');
  });
});
