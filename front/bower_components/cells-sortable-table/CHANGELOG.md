# Changelog

<!-- Document the breaking changes when updating the component as major version.
As well as the necessary changes to upgrade the component (migration guide type).
-->

## v0.2.0

### Breaking changes

- `sort` renamed to `sortOrder`
- `sortButtons` renamed to `showSortButtons`
- `sort-changed` event detail changed to `sort: { order: <sortOrder>, property: <sortBy>}`
