# &lt;cells-sortable-table&gt;

![cells-sortable-table screenshot](cells-sortable-table.png)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)
![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=docs#/component/cells-sortable-table)

`<cells-sortable-table>` is a table that allows to sort its rows ascending or descending by clicking on header sort buttons.
The initial sorting can be set by setting the attribute `sort-by` with the `id` of a table column header. The sorting method, by default descending, can be set by
setting the `sort-order` attribute to a valid value (`ascending` or `descending`).

Table rows and columns are passed using the attributes `rows` and `column-headers` respectively. Both attributes expect an array.

The `label` property of each object of the `rows` and `columnHeaders` properties allows to use HTML for the cases where you need to include paragraphs or headings (complex tables).
The component implements the __i18n behavior__, so you can pass translation keys as values for table headers or cells.

## Data model for `column-headers`

Each object of the array corresponds to a column header (`thead > tr > th`).
The `label` key admits HTML.

```js
[{
  label: 'Header text', // Can be a string with or without HTML or a translation key (string).
  width: '10%', // Optional style for `thead` table headers
  sortable: true, // Set to `true` to allow sorting by this header
  id: 'unique-header-id' // Unique ID required when the header is set as `sortable`. This ID can be used to set an initial `sort-by` attribute.
}];
```

To set an __empty table header__, use an object with a empty label:

```js
[{
  label: ''
}];
```

## Data model for `rows`

Each object of the array corresponds to a table row (`tbody > tr`).
Each object of the `cells` array corresponds to a table cell (`tbody > tr > td`).
The `label` key admits HTML.

```js
[
  {
    cells: [
      {label: 'orange', isHeader: true}, // Set `isHeader` to true to render a row header (`th`)
      {label: 77.1},
      {label: 1.8},
      {label: 5.7},
      {label: 95.8}
    ]
  }
]
```

## Highlighting columns and rows

To hightlight one or more columns and rows, use the attribute `highlight-columns` and `highlight-rows` respectively with the index of the colum(s) or row(s) to highlight.
Both attributes admit a number, for a single index, or an array for a list of indexes.

Example:

```html
<cells-sortable-table
  highlight-columns='[2, 4]'
  highlight-rows="0">
</cells-sortable-table>
```

## Table styles

There are three __available classes__ to set the style according to [UI Studio guidelines for tables](https://ui.bbva.com/web_modules/tables/).
For now, there aren't styles for responsive tables (mobile).

| Style | Class name |
|:------|:-----------|
| Complex table | `complex` |
| Comparison table | `comparison` |
| Simple table | `simple` |

When none of these classes are used, the table has __minimal styles__ (no padding, borders, etc.)

## Icons

Since this component uses icons, it will need an [iconset](https://bbva-devplatform.appspot.com/en-us/developers/engines/cells/documentation/best-practices/cells-icons) in your project as an [application level dependency](https://bbva-devplatform.appspot.com/en-us/developers/engines/cells/documentation/advanced-guides/components/application-level-dependencies).
In fact, this component uses an iconset in its demo.

## Styling

### Content inside Shadow DOM

To style custom HTML provided as value for table cells you will need to use a shared style with the id `cells-sortable-table-shared-styles`.

Example:

```html
<dom-module id="cells-sortable-table-shared-styles">
  <template>
    <style>
      /* applied to h2 tags inside table cells */
      h2 {
        margin-top: 0;
        color: #121212;
      }
    </style>
  </template>
</dom-module>
```

The following custom properties and mixins are available for styling:

| Custom Property                                                      | Description            | Default |
| :------------------------------------------------------------------- | :--------------------- | :------ |
| --cells-fontDefault                                                  | Font family of :host   | sans-serif |
| --cells-sortable-table                                               | Mixin applied to :host | {} |
| --cells-sortable-table-caption                                       | Mixin applied to `caption` | {} |
| --cells-sortable-table-cell                                          | Mixin applied to cells (`th` and `td`) | {} |
| --cells-sortable-table-cell-text-align                               | `text-align` applied to cells | left  |
| --cells-sortable-table-th-color                                      | Shared color for all styled (with class) `th` | var(--bbva-600, #121212) |
| --cells-sortable-table-td-color                                      | Shared color for all styled (with class) `td` | var(--bbva-500, #666666) |
| --cells-sortable-table-border-color-dark                             | Shared dark border color for styled (with class) tables | var(--bbva-500, #666666) |
| --cells-sortable-table-border-color-light                            | Shared light border color for styled (with class) tables | var(--bbva-400, #BDBDBD) |
| --cells-sortable-table-cell-font-size                                | Shared cells `font-size` for styled (with class) tables | 15px |
| --cells-sortable-table-comparison-cell                               | Mixin applied to cells in "comparison" table | {} |
| --cells-sortable-table-comparison-cell-font-size                     | `font-size` applied to cells in "comparision" table | var(--cells-sortable-table-cell-font-size) |
| --cells-sortable-table-comparison-cell-horizontal-padding            | Horizontal padding (left and right) applied to cells in "comparision" table | 32px   |
| --cells-sortable-table-comparison-table                              | Empty mixin applied to `table` in "comparison" table | {} |
| --cells-sortable-table-comparison-table-border-color                 | Border color applied to `table` in "comparison" table  | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-comparison-tbody-cell                         | Empty mixin applied to `tbody` cells in "comparison" table  | {} |
| --cells-sortable-table-comparison-tbody-cell-border-color            | Border color applied to `tbody` cells in "comparison" table | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-comparison-tbody-cell-vertical-padding        | Vertical padding (top and bottom) applied to `tbody` cells in "comparison" table | 8px |
| --cells-sortable-table-comparison-tbody-first-row-cell               | Empty mixin applied to cells of first `tbody` row in "comparsison" table | {} |
| --cells-sortable-table-comparison-tbody-first-row-cell-padding-top   | `padding-top` applied to cells of first `tbody` row in "comparsison" table | 24px |
| --cells-sortable-table-comparison-tbody-last-row-cell                | Empty mixin applied to cells of last `tbody` row in "comparsison" table  | {} |
| --cells-sortable-table-comparison-tbody-last-row-cell-padding-bottom | `padding-bottom` applied to cells of last `tbody` row in "comparsison" table  | 24px |
| --cells-sortable-table-comparison-td                                 | Empty mixin applied to `td` in "comparision" table | {} |
| --cells-sortable-table-comparison-td-color                           | Color applied to `td` in "comparsison" table | var(--cells-sortable-table-td-color) |
| --cells-sortable-table-comparison-th                                 | Empty mixin applied to `th` in "comparison" table | {} |
| --cells-sortable-table-comparison-th-color                           | Color applied to `th` in "comparison" table | var(--cells-sortable-table-th-color) |
| --cells-sortable-table-comparison-thead-cell                         | Empty mixin applied to `thead` cells in "comparison" table | {} |
| --cells-sortable-table-comparison-thead-cell-border-color            | Border color applied to `thead` cells in "comparison" table  | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-comparison-thead-cell-vertical-padding        | Vertical padding applied to `thead` cells in "comparison" table  | 24px |
| --cells-sortable-table-complex-cell                                  | Empty mixin applied to cells in "complex" table | {} |
| --cells-sortable-table-complex-cell-border-color                     | Border color applied to cells in "complex" table | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-complex-cell-font-size                        | `font-size` applied to cells in "complex" table  | var(--cells-sortable-table-cell-font-size) |
| --cells-sortable-table-complex-cell-padding                          | `padding` applied to cells in "complex" table  | 24px 32px |
| --cells-sortable-table-complex-table                                 | Empty mixin applied to `table` in "complex" table | {} |
| --cells-sortable-table-complex-table-border-color                    | Border color applied to `table` in "complex" table | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-complex-td                                    | Empty mixin applied to `td` in "complex" table | {} |
| --cells-sortable-table-complex-td-color                              | Color applied to `td` in "complex" table | var(--cells-sortable-table-td-color) |
| --cells-sortable-table-complex-th                                    | Empty mixin applied to `th` in "complex" table | {} |
| --cells-sortable-table-complex-th-color                              | Color applied to `th` in "complex" table | var(--cells-sortable-table-th-color) |
| --cells-sortable-table-highlight                                     | Empty mixin applied to highlighted cells | {} |
| --cells-sortable-table-highlight-bg-color                            | `background-color` of highlighted cells | var(--bbva-white-yellow, #FEF5DC) |
| --cells-sortable-table-icon-size                                     | Size used for `width` and `height` of the sort icons | 20px |
| --cells-sortable-table-simple-cell                                   | Empty mixin applied to cells in "simple" table | {} |
| --cells-sortable-table-simple-cells-font-size                        | `font-size` applied to cells in "simple" table | var(--cells-sortable-table-cell-font-size) |
| --cells-sortable-table-simple-cells-padding-horizontal               | Horizontal padding applied to cells in "simple" table | 32px |
| --cells-sortable-table-simple-tbody-cell                             | Empty mixin applied to `tbody` cells in "simple" table | {} |
| --cells-sortable-table-simple-tbody-cell-border-color                | Border color applied to `tbody` cells in "simple" table | var(--cells-sortable-table-border-color-light) |
| --cells-sortable-table-simple-tbody-cell-vertical-padding            | Vertical padding applied to `tbody` cells in "simple" table | 24px |
| --cells-sortable-table-simple-td                                     | Empty mixin applied to `td` in "simple" table | {}  |
| --cells-sortable-table-simple-td-color                               | Color applied to `td` in "simple" table | var(--cells-sortable-table-td-color) |
| --cells-sortable-table-simple-th                                     | Empty mixin applied to `th` in "simple" table | {} |
| --cells-sortable-table-simple-th-color                               | Color applied to `th` in "simple" table | var(--cells-sortable-table-th-color) |
| --cells-sortable-table-simple-thead-cell                             | Empty mixin applied to `thead` cells in "simple" table | {} |
| --cells-sortable-table-simple-thead-cell-border-color                | Border color applied to `thead` cells in "simple" table  | var(--cells-sortable-table-border-color-dark) |
| --cells-sortable-table-simple-thead-cell-vertical-padding            | Vertical padding applied to `thead` cells in "simple" table | 16px |
| --cells-sortable-table-table                                         | Empty mixin applied to `table` | {} |
| --cells-sortable-table-tbody-tr-even                                 | Empty mixin applied to `tr:nth-child(even)` in `tbody` | {} |
| --cells-sortable-table-tbody-tr-odd                                  | Empty mixin applied to `tr:nth-child(odd)` in `tbody` | {} |
| --cells-sortable-table-tbody-td                                      | Empty mixin applied to `td` in `tbody` | {} |
| --cells-sortable-table-tbody-th                                      | Empty mixin applied to `th` in `tbody` | {} |
| --cells-sortable-table-td                                            | Empty mixin applied to all `td` | {} |
| --cells-sortable-table-td-color                                      | Shared color for all styled (with class) `td` | var(--bbva-500, #666666) |
| --cells-sortable-table-th                                            | Empty mixin applied to `th` | {} |
| --cells-sortable-table-th-color                                      | Shared color for all styled (with class) `th` | var(--bbva-600, #121212) |
| --cells-sortable-table-th-content                                    | Empty mixin applied to `th` content (text and sort buttons)  | {} |
| --cells-sortable-table-th-content-text                               | Empty mixin applied to `th` content text | {} |
| --cells-sortable-table-thead-th                                      | Empty mixin applied to `th` in `thead` | {} |
| --cells-sortable-table-toggle                                        | Empty mixin applied to toggle buttons wrapper (sort) | {} |
| --cells-sortable-table-toggle-btn                                    | Empty mixin applied to toggle button  | {} |
| --cells-sortable-table-toggle-btn-focused                            | Empty mixin applied to focused toggle button  | {} |
| --cells-sortable-table-toggle-btn-selected                           | Empty mixin applied to selected toggle button | {} |
| --cells-sortable-table-toggle-btn-unfocused                          | Empty mixin applied to unfocused toggle button | {}  |
| --cells-sortable-table-toggle-icon                                   | Empty mixin applied to selected toggle icon | {}  |
