class CellsSortableTable extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {
  static get is() {
    return 'cells-sortable-table';
  }

  static get properties() {
    return {
      /**
       * List of object configs for column headers.
       *
       * Format:
       * ```json
       * {
       *   label: "Your column heading",
       *   id: 'unique-id', // Required when you set the column as sortable.
       *   sortable: true, // sets if the column can be sorted. Setting to true requires the `id` property.
       *   width: '10%', // Optional style for `thead` table headers
       * }
       * ```
       */
      columnHeaders: Array,

      /**
       * List of cells objects for table rows.
       *
       * Format:
       * ```json
       * {
       *   cells: [
       *     {label: 'Row heading', isHeader: true},
       *     {label: 142},
       *     {label: 70},
       *     {label: '49.30%'},
       *     {label: 33},
       *     {label: '31.42%'}
       *   ]
       * }
       * ```
       */
      rows: Array,

      _rows: {
        type: Array,
        computed: '_computeRows(rows, sortBy, sortOrder)'
      },

      /**
       * ID of the column header used to sort the table.
       */
      sortBy: String,

      /**
       * Sorting direction. Possible values are `ascending` or `descending`.
       */
      sortOrder: {
        type: String,
        value: 'descending'
      },

      /**
       * Set to true to display buttons for changing the sort direction in sortable columns.
       */
      showSortButtons: {
        type: Boolean,
        value: false
      },

      /**
       * Icon ID of the sort descending (up) button.
       */
      iconSortDescending: {
        type: String,
        value: 'coronita:fold'
      },

      /**
       * Icon ID of the sort ascending (down) button.
       */
      iconSortAscending: {
        type: String,
        value: 'coronita:unfold'
      },

      /**
       * Index of the column(s) to highlight. The value can be Number or Array.
       * Example: `highlight-columns="5"` or `highlight-columns='[2, 5]'`
       */
      highlightColumns: Array,

      /**
       * Index of the row(s) to hightlight. The value can be Number or Array.
       * Example: `highlight-rows="5"` or `highlight-rows='[2, 5]'`
       */
      highlightRows: Array,

      /**
       * Table summary.
       * https://www.w3.org/TR/WCAG20-TECHS/H73.html
       */
      summary: String,

      /**
       * Table caption.
       * https://www.w3.org/TR/WCAG20-TECHS/H73.html
       */
      caption: String,

      _highlightRows: {
        type: Array,
        computed: '_toArray(highlightRows)'
      },

      _highlightColumns: {
        type: Array,
        computed: '_toArray(highlightColumns)'
      },

      _errorType: {
        type: String,
        observer: '_logError'
      }
    };
  }

  get _errorMessages() {
    return {
      INVALID_SORT_ORDER: `"${this.sortOrder}" is not a valid "sortOrder" value. It should be "ascending" or "descending". The table is not sorted.`,
      INVALID_SORT_BY: `"sortBy" "${this.sortBy}" not found in "columnHeaders". The table is not sorted.`
    };
  }

  _computeRows(rows, sortBy) {
    if (!sortBy || !this.columnHeaders) {
      return rows;
    }

    if (this._tableCanBeSorted()) {
      return this._getSortedRows();
    }

    // uncheck all radios in case the table has been sorted previously
    this._uncheckAllRadios();
    return rows;
  }

  _tableCanBeSorted() {
    this._indexOfSortByColumn = this.columnHeaders.findIndex(cell => cell.id === this.sortBy);
    this._setErrorType(this.sortOrder, this._indexOfSortByColumn);
    const hasNoErrors = !this._errorType;
    return hasNoErrors;
  }

  _getSortedRows() {
    const cellLabel = this._getCellProperty(this._indexOfSortByColumn, 'label');
    return [ ...this.rows ].sort(this._compare(cellLabel, this.sortOrder));
  }

  _getCellProperty(index, property) {
    return (item) => item.cells[index][property];
  }

  /**
   * Sorting function used for sorting table columnns.
   * @param  {Function} property  Function that returns the property used to sort
   * @param  {String} order Sorting method: 'ascending' or 'descending'
   */
  _compare(property, order) {
    const sortOrder = (order === 'descending') ? 1 : -1;
    return (a, b) => {
      const result = (property(a) < property(b)) ? 1 : (property(a) > property(b)) ? -1 : 0;
      return result * sortOrder;
    };
  }

  _setErrorType(sortOrder, indexOfSortByColumn) {
    if (indexOfSortByColumn === -1) {
      this._errorType = 'INVALID_SORT_BY';
      return;
    }

    if (['ascending', 'descending'].indexOf(sortOrder) === -1) {
      this._errorType = 'INVALID_SORT_ORDER';
      return;
    }

    this._errorType = null;
  }

  _logError(errorType) {
    if (errorType) {
      console.warn(this._errorMessages[errorType]);
    }
  }

  _setSorting(e) {
    this.sortBy = e.model.item.id;
    this.sortOrder = e.currentTarget.value;
    this._setCheckedRadio(e.currentTarget);

    /**
     * @event sort-changed
     * Fired after chaging the sort direction ('ascending' or 'descending')
     * @param {Object} detail {sort} Event.detail
     * @param {String} event.detail.sort.order Selected "sortOrder" ("ascending" or "descending").
     * @param {String} event.detail.sort.property ID of the header selected for sorting.
     */
    const sort = { order: this.sortOrder, property: this.sortBy };
    this.dispatchEvent(new CustomEvent('sort-changed', {
      bubbles: true,
      composed: true,
      detail: { sort }
    }));
  }

  _computeSortButtonsVisibility(sortable, showSortButtons) {
    return (sortable && showSortButtons);
  }

  _setInitialRadioChecked() {
    if (!this.sortBy || !this.showSortButtons) {
      return;
    }

    Polymer.dom.flush(); // Ensure dom-if inside dom-repeat has been rendered
    const radio = this.shadowRoot.querySelector(`[data-id="${this.sortBy}"][value="${this.sortOrder}"]`);

    if (radio) {
      radio.setAttribute('aria-checked', 'true');
    }
  }

  _setCheckedRadio(radio) {
    this._uncheckAllRadios();
    radio.setAttribute('aria-checked', 'true');
  }

  _uncheckAllRadios() {
    const radios = this.shadowRoot.querySelectorAll('.toggle__btn');
    for (const radio of radios) {
      radio.setAttribute('aria-checked', 'false');
    }
  }

  /**
   * Sets an inline style attribute or removes it.
   */
  _computeWidth(width) {
    return width ? `width: ${width};` : false;
  }

  _computeHide(hide) {
    return hide ? true : false;
  }

  _toArray(value) {
    return Array.isArray(value) ? value : [ value ];
  }

  _computeHighlightClass(index, highlighted) {
    if (!highlighted) {
      return false;
    }

    const indexInCell = (a) => a === index;
    return highlighted.some(indexInCell) ? `highlight highlight-${index}` : '';
  }
}

customElements.define(CellsSortableTable.is, CellsSortableTable);
